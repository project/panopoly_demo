<?php

/**
 * @file
 * Install and update hooks for Panopoly Demo.
 */

/**
 * Implements hook_install().
 */
function panopoly_demo_install() {
  $migration_ids = [
    'panopoly_demo_node',
    'panopoly_demo_menu',
  ];
  /** @var \Drupal\panopoly_core\MigrateHelper $migrate_helper */
  $migrate_helper = \Drupal::service('panopoly_core.migrate_helper');
  $migrate_helper->import($migration_ids);

  // Point the front page to our landing page demo.
  _panopoly_demo_set_front_page('/demo');

  // Install database search by default.
  /** @var \Drupal\Core\Extension\ModuleInstallerInterface $module_installer */
  $module_installer = \Drupal::service('module_installer');
  $module_installer->install(['panopoly_search_db']);
}

/**
 * Implements hook_uninstall().
 */
function panopoly_demo_uninstall() {
  $migration_ids = [
    'panopoly_demo_node',
    'panopoly_demo_menu',
  ];
  /** @var \Drupal\panopoly_core\MigrateHelper $migrate_helper */
  $migrate_helper = \Drupal::service('panopoly_core.migrate_helper');
  $migrate_helper->rollback($migration_ids);

  // Reset the homepage.
  _panopoly_demo_set_front_page('/node');
}

/**
 * Helper to set the front page.
 *
 * @param string $path
 *   The path for the front page.
 */
function _panopoly_demo_set_front_page($path) {
  $site_config = \Drupal::configFactory()->getEditable('system.site');
  $site_page_settings = $site_config->get('page');

  // Only set the homepage if the site does not doesn't already have custom
  // front page.
  if (in_array($site_page_settings['front'], ['', '/user/login', '/node'])) {
    $site_page_settings['front'] = $path;
    $site_config->set('page', $site_page_settings);
    $site_config->save();
  }
}

/**
 * Implements hook_update_dependencies().
 */
function panopoly_demo_update_dependencies() {
  $dependencies = [];
  $dependencies['panopoly_demo'][8001] = [
    'panopoly' => 8001,
  ];
  return $dependencies;
}

/**
 * Implements hook_update_last_removed().
 */
function panopoly_demo_update_last_removed() {
  return 8002;
}
